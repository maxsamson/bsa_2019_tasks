﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.Shared.SelectionModels
{
    public class JobsCount
    {
        public ProjectDTO Project { get; set; }
        public int Count { get; set; }
    }
}
