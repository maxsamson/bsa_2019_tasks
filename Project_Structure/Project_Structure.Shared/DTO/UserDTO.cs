﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Project_Structure.Shared.DTO
{
    public class UserDTO
    {
        public int Id { get; set; }
        public string First_Name { get; set; }
        public string Last_Name { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Registered_At { get; set; }
        public List<JobDTO> Jobs { get; set; }
        public List<ProjectDTO> Projects { get; set; }
        public int Team_Id { get; set; }

        public UserDTO()
        {
            Jobs = new List<JobDTO>();
            Projects = new List<ProjectDTO>();
        }
    }
}
