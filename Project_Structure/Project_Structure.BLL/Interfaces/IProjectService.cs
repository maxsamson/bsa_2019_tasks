﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface IProjectService
    {
        Task<List<ProjectDTO>> GetAll();

        Task<ProjectDTO> GetById(int id);

        Task<int> Create(ProjectDTO project);

        Task Update(int id, ProjectDTO project);

        Task Delete(ProjectDTO project);

        Task DeleteById(int id);
    }
}
