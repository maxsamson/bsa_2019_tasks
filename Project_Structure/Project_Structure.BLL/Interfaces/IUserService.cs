﻿using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface IUserService
    {
        Task<List<UserDTO>> GetAll();

        Task<UserDTO> GetById(int id);

        Task<int> Create(UserDTO user);

        Task Update(int id, UserDTO user);

        Task Delete(UserDTO user);

        Task DeleteById(int id);
    }
}
