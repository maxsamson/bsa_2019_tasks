﻿using DAL.Models;
using Project_Structure.Shared.DTO;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Interfaces
{
    public interface IMapper
    {
        ProjectDTO MapProject(Project value);
        Task<Project> MapProject(ProjectDTO value);
        List<ProjectDTO> MapProjects(List<Project> valueList);
        Task<List<Project>> MapProjects(List<ProjectDTO> valueList);

        JobDTO MapJob(Job value);
        Task<Job> MapJob(JobDTO value);
        List<JobDTO> MapJobs(List<Job> valueList);
        Task<List<Job>> MapJobs(List<JobDTO> valueList);

        TeamDTO MapTeam(Team value);
        Task<Team> MapTeam(TeamDTO value);
        List<TeamDTO> MapTeams(List<Team> valueList);
        Task<List<Team>> MapTeams(List<TeamDTO> valueList);

        UserDTO MapUser(User value);
        Task<User> MapUser(UserDTO value);
        List<UserDTO> MapUsers(List<User> valueList);
        Task<List<User>> MapUsers(List<UserDTO> valueList);
    }
}
