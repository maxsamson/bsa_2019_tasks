﻿using DAL.Models;
using DAL.Repositories;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.SelectionModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class SelectionService : ISelectionService
    {
        private readonly ProjectRepository projectRepo;
        private readonly JobRepository JobRepo;
        private readonly TeamRepository teamRepo;
        private readonly UserRepository userRepo;
        private readonly IMapper mapper;
        readonly QueueService queueService;

        public SelectionService(ProjectRepository projectRepo, JobRepository JobRepo, TeamRepository teamRepo, 
            UserRepository userRepo, IMapper mapper, QueueService queueService)
        {
            this.projectRepo = projectRepo;
            this.JobRepo = JobRepo;
            this.teamRepo = teamRepo;
            this.userRepo = userRepo;
            this.mapper = mapper;
            this.queueService = queueService;
        }

        public async Task<List<ProjectDTO>> CreateHierarhy()
        {
            try
            {
                queueService.PostValueWorker("Hierarhy creation was triggered");

                return mapper.MapProjects(await projectRepo.GetAll());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public async Task<List<JobsCount>> JobsCount(int user_Id)
        {
            try
            {
                queueService.PostValueWorker($"JobsCount with user_Id={user_Id} was triggered");

                return (await projectRepo.GetAll())
                    .Where(p => p.Author.Id == user_Id)
                    .Select(p => new JobsCount
                    {
                        Project = mapper.MapProject(p),
                        Count = p.Jobs.Count()
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return null;
            }
        }

        public async Task<List<JobDTO>> JobsWithNameLess45Symbols(int user_Id)
        {
            try
            {
                queueService.PostValueWorker($"JobsWithNameLess45Symbols with user_Id={user_Id} was triggered");

                return mapper.MapJobs((await userRepo.GetAll()).Where(item => item.Id == user_Id).SelectMany(item => item.Jobs.Where(t => t.Name.Length < 45)).ToList());
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<JobsFinished2019>> JobsFinished2019(int user_Id)
        {
            try
            {
                queueService.PostValueWorker($"JobsFinished2019 with user_Id={user_Id} was triggered");

                return (await userRepo.GetAll())
                    .Where(item => item.Id == user_Id)
                    .SelectMany(item => item.Jobs.Where(t => t.State == State.Finished && t.Finished_At.Year == 2019))
                    .Select(t => new JobsFinished2019()
                    {
                        Id = t.Id,
                        Name = t.Name
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<Teams12yearsGroup>> Teams12yearsGroup()
        {
            try
            {
                queueService.PostValueWorker($"Teams12yearsGroup was triggered");

                return (await teamRepo.GetAll())
                    .Where(t => t.Users.Count > 0 && t.Users.All(u => DateTime.Now.Year - u.Birthday.Year > 12))
                    .Select(item => new Teams12yearsGroup
                    {
                        Id = item.Id,
                        Name = item.Name,
                        Users = mapper.MapUsers(item.Users)
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<UsersACSJobsDSC>> UsersACSJobsDSC()
        {
            try
            {
                queueService.PostValueWorker($"UsersACSJobsDSC was triggered");

                return (await userRepo.GetAll())
                    .OrderBy(u => u.First_Name)
                    .Select(item => new UsersACSJobsDSC
                    {
                        User = mapper.MapUser(item),
                        Jobs = mapper.MapJobs(item.Jobs.OrderByDescending(t => t.Name.Length).ToList())
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<UserStruct>> UserStruct(int user_Id)
        {
            try
            {
                queueService.PostValueWorker($"UserStruct with user_Id={user_Id} was triggered");

                return (await userRepo.GetAll())
                    .Where(item => item.Id == user_Id)
                    .Select(item => new UserStruct
                    {
                        User = mapper.MapUser(item),
                        LastProject = mapper.MapProject(item.Projects.OrderByDescending(p => p.Created_At).First()),
                        LastProjectJobsCount = item.Projects.OrderByDescending(p => p.Created_At).First().Jobs.Count(),
                        NotFinishedCanceledJobs = mapper.MapJobs(item.Jobs.Where(t => t.State != State.Finished).ToList()),
                        MaxDurationJob = mapper.MapJob(item.Jobs.OrderBy(t => t.Created_At).OrderByDescending(t => t.Finished_At).First())
                    }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<ProjectStruct>> ProjectStruct(int proj_Id)
        {
            try
            {
                queueService.PostValueWorker($"ProjectStruct with project_Id={proj_Id} was triggered");

                return (await projectRepo.GetAll())
                    .Where(item => item.Id == proj_Id)
                    .Select(item => new ProjectStruct
                        {
                            Project = mapper.MapProject(item),
                            LongestJob = mapper.MapJob(item.Jobs.OrderByDescending(t => t.Description).FirstOrDefault()),
                            ShortestJob = mapper.MapJob(item.Jobs.OrderBy(t => t.Name).FirstOrDefault()),
                            UsersCount = item.Description.Length > 25 || item.Jobs.Count < 3 ? item.Team.Users.Count() : 0
                        }).ToList();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                return null;
            }
        }

        public async Task<List<Message>> GetMessages(string path)
        {
            try
            {
                queueService.PostValueWorker($"GetMessages was triggered");

                List<Message> result = new List<Message>();

                using (StreamReader sr = new StreamReader(path, System.Text.Encoding.Default))
                {
                    string line;
                    while ((line = sr.ReadLine()) != null)
                    {
                        var list = line.Split("\t");
                        result.Add(new Message()
                        {
                            Create_At = Convert.ToDateTime(list[0]),
                            Text = list[1]
                        });
                    }
                }

                return result;
            }
            catch(Exception)
            {
                return null;
            }
        }
    }
}
