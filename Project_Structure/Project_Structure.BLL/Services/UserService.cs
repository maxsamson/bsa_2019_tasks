﻿using DAL.Repositories;
using FluentValidation;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class UserService : IUserService
    {
        private readonly UserRepository repository;
        private readonly IMapper mapper;
        private readonly AbstractValidator<UserDTO> validator;
        readonly QueueService queueService;

        public UserService(UserRepository userRepository, IMapper mapper, AbstractValidator<UserDTO> validator, QueueService queueService)
        {
            this.repository = userRepository;
            this.mapper = mapper;
            this.validator = validator;
            this.queueService = queueService;
        }

        public async Task<List<UserDTO>> GetAll()
        {
            queueService.PostValueWorker($"Loading all users was triggered");

            var result = new List<UserDTO>();
            foreach (var item in await repository.GetAll())
            {
                result.Add(mapper.MapUser(item));
            }
            return result;
        }

        public async Task<UserDTO> GetById(int id)
        {
            queueService.PostValueWorker($"Loading user with id={id} was triggered");

            return mapper.MapUser(await repository.GetById(id));
        }

        public async Task<int> Create(UserDTO user)
        {
            queueService.PostValueWorker($"User creation was triggered");

            var validationResult = validator.Validate(user);
            if (validationResult.IsValid)
                return await repository.Create(await mapper.MapUser(user));
            else
                throw new ValidationException(validationResult.Errors);
        }

        public async Task Update(int id, UserDTO user)
        {
            queueService.PostValueWorker($"User modification with id={id} was triggered");

            var validationResult = validator.Validate(user);
            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors);
            try
            {
                await repository.Update(await mapper.MapUser(user));
            }
            catch (ArgumentNullException)
            {
                throw new NotFoundException();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(UserDTO user)
        {
            queueService.PostValueWorker($"Deleting user was triggered");

            await repository.Delete(await mapper.MapUser(user));
        }

        public async Task DeleteById(int id)
        {
            queueService.PostValueWorker($"Deleting user with id={id} was triggered");

            await repository.DeleteById(id);
        }
    }
}
