﻿using DAL.Repositories;
using FluentValidation;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class TeamService : ITeamService
    {
        private readonly TeamRepository repository;
        private readonly IMapper mapper;
        private readonly AbstractValidator<TeamDTO> validator;
        readonly QueueService queueService;

        public TeamService(TeamRepository teamRepository, IMapper mapper, AbstractValidator<TeamDTO> validator, QueueService queueService)
        {
            this.repository = teamRepository;
            this.mapper = mapper;
            this.validator = validator;
            this.queueService = queueService;
        }

        public async Task<List<TeamDTO>> GetAll()
        {
            queueService.PostValueWorker($"Loading all teams was triggered");

            var result = new List<TeamDTO>();
            foreach (var item in await repository.GetAll())
            {
                result.Add(mapper.MapTeam(item));
            }
            return result;
        }

        public async Task<TeamDTO> GetById(int id)
        {
            queueService.PostValueWorker($"Loading team with id={id} was triggered");

            return mapper.MapTeam(await repository.GetById(id));
        }

        public async Task<int> Create(TeamDTO team)
        {
            queueService.PostValueWorker($"Team creation was triggered");

            var validationResult = validator.Validate(team);
            if (validationResult.IsValid)
                return await repository.Create(await mapper.MapTeam(team));
            else
                throw new ValidationException(validationResult.Errors);
        }

        public async Task Update(int id, TeamDTO team)
        {
            queueService.PostValueWorker($"Team modification with id={id} was triggered");

            var validationResult = validator.Validate(team);
            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors);
            try
            {
                await repository.Update(await mapper.MapTeam(team));
            }
            catch (ArgumentNullException)
            {
                throw new NotFoundException();
            }
            catch (Exception)
            {
                throw;
            }
        }

        public async Task Delete(TeamDTO team)
        {
            queueService.PostValueWorker($"Deleting team was triggered");

            await repository.Delete(await mapper.MapTeam(team));
        }

        public async Task DeleteById(int id)
        {
            queueService.PostValueWorker($"Deleting team with id={id} was triggered");

            await repository.DeleteById(id);
        }
    }
}
