﻿using DAL.Repositories;
using FluentValidation;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class JobService : IJobService
    {
        private readonly JobRepository repository;
        private readonly IMapper mapper;
        private readonly AbstractValidator<JobDTO> validator;
        readonly QueueService queueService;

        public JobService(JobRepository JobRepository, IMapper mapper, AbstractValidator<JobDTO> validator, QueueService queueService)
        {
            this.repository = JobRepository;
            this.mapper = mapper;
            this.validator = validator;
            this.queueService = queueService;
        }

        public async Task<List<JobDTO>> GetAll()
        {
            queueService.PostValueWorker($"Loading all jobs was triggered");

            var result = new List<JobDTO>();
            foreach (var item in await repository.GetAll())
            {
                result.Add(mapper.MapJob(item));
            }
            return result;
        }

        public async Task<JobDTO> GetById(int id)
        {
            queueService.PostValueWorker($"Loading Job with id={id} was triggered");

            return mapper.MapJob(await repository.GetById(id));
        }

        public async Task<int> Create(JobDTO Job)
        {
            queueService.PostValueWorker($"Job creation was triggered");

            var validationResult = validator.Validate(Job);
            if (validationResult.IsValid)
                return await repository.Create(await mapper.MapJob(Job));
            else
                throw new ValidationException(validationResult.Errors);
        }

        public async Task Update(int id, JobDTO Job)
        {
            queueService.PostValueWorker($"Job modification with id={id} was triggered");

            Job.Id = id;

            var validationResult = validator.Validate(Job);
            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors);
            try
            {
                await repository.Update(await mapper.MapJob(Job));
            }
            catch (ArgumentNullException)
            {
                throw new NotFoundException();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public async Task Delete(JobDTO Job)
        {
            queueService.PostValueWorker($"Deleting Job was triggered");

            await repository.Delete(await mapper.MapJob(Job));
        }

        public async Task DeleteById(int id)
        {
            queueService.PostValueWorker($"Deleting Job with id={id} was triggered");

            await repository.DeleteById(id);
        }
    }
}
