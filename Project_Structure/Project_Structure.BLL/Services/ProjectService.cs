﻿using DAL.Repositories;
using FluentValidation;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Project_Structure.BLL.Services
{
    public class ProjectService : IProjectService
    {
        private readonly ProjectRepository repository;
        private readonly IMapper mapper;
        private readonly AbstractValidator<ProjectDTO> validator;
        readonly QueueService queueService;

        public ProjectService(ProjectRepository projectRepository, IMapper mapper, AbstractValidator<ProjectDTO> validator, QueueService queueService)
        {
            this.repository = projectRepository;
            this.mapper = mapper;
            this.validator = validator;
            this.queueService = queueService;
        }

        public async Task<List<ProjectDTO>> GetAll()
        {
            queueService.PostValueWorker($"Loading all projects was triggered");

            var result = new List<ProjectDTO>();
            foreach (var item in await repository.GetAll())
            {
                result.Add(mapper.MapProject(item));
            }
            return result;
        }

        public async Task<ProjectDTO> GetById(int id)
        {
            queueService.PostValueWorker($"Loading project with id={id} was triggered");

            return mapper.MapProject(await repository.GetById(id));
        }

        public async Task<int> Create(ProjectDTO project)
        {
            queueService.PostValueWorker($"Project creation was triggered");

            var validationResult = validator.Validate(project);
            if (validationResult.IsValid)
                return await repository.Create(await mapper.MapProject(project));
            else
                throw new ValidationException(validationResult.Errors);
        }

        public async Task Update(int id, ProjectDTO project)
        {
            queueService.PostValueWorker($"Project modification with id={id} was triggered");

            var validationResult = validator.Validate(project);
            if (!validationResult.IsValid)
                throw new ValidationException(validationResult.Errors);
            try
            {
                await repository.Update(await mapper.MapProject(project));
            }
            catch (ArgumentNullException)
            {
                throw new NotFoundException();
            }
        }

        public async Task Delete(ProjectDTO project)
        {
            queueService.PostValueWorker($"Deleting project was triggered");

            await repository.Delete(await mapper.MapProject(project));
        }

        public async Task DeleteById(int id)
        {
            queueService.PostValueWorker($"Deleting project with id={id} was triggered");

            await repository.DeleteById(id);
        }
    }
}
