﻿using System;
using System.Threading.Tasks;
using FluentValidation;
using Microsoft.AspNetCore.Mvc;
using Nito.AsyncEx;
using Project_Structure.BLL.Interfaces;
using Project_Structure.Shared.DTO;
using Project_Structure.Shared.Exceptions;

namespace Project_Structure.Controllers
{
    [Route("api/Projects")]
    [ApiController]
    public class ProjectsController : ControllerBase
    {
        private readonly AsyncLock _asyncLock = new AsyncLock();
        readonly IProjectService service;
        
        public ProjectsController(IProjectService projectService)
        {
            service = projectService;
        }

        // GET: api/Projects
        [HttpGet]
        public async Task<IActionResult> Get()
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(await service.GetAll());
                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                return NotFound();
            }
        }

        // GET: api/Projects/5
        [HttpGet("{id}")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.GetById(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // POST: v1/api/Projects
        [HttpPost]
        public async Task<IActionResult> Post([FromBody]ProjectDTO project)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.Create(project));
                }
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // PUT: v1/api/Projects/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, [FromBody] ProjectDTO project)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.Update(id, project));
                }
            }
            catch (NotFoundException)
            {
                return NotFound();
            }
            catch (ValidationException e)
            {
                return BadRequest(e.Message);
            }
            catch (Exception)
            {
                return BadRequest();
            }
        }

        // DELETE: v1/api/Projects/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.DeleteById(id));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }

        // DELETE: v1/api/Projects
        [HttpDelete]
        public async Task<IActionResult> Delete([FromBody] ProjectDTO project)
        {
            try
            {
                using (await _asyncLock.LockAsync())
                {
                    return Ok(service.Delete(project));
                }
            }
            catch (Exception)
            {
                return NotFound();
            }
        }
    }
}