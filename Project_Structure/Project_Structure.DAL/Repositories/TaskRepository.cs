﻿using DAL.DBInfrastructure;
using DAL.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.Repositories
{
    public class JobRepository : BaseRepository<Job>
    {
        public JobRepository(ProjectDbContext dBContext) : base(dBContext)
        {
            
        }
    }
}
