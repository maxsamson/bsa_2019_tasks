﻿using DAL.DBInfrastructure;
using DAL.Interfaces;
using DAL.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace DAL.Repositories
{
    public class BaseRepository<TEntity> : IRepository<TEntity> where TEntity : Entity
    {
        protected readonly ProjectDbContext projectDbContext;

        public BaseRepository(ProjectDbContext dbContext)
        {
            projectDbContext = dbContext;

            DataSource.Initialize(projectDbContext);
        }

        public virtual async Task<int> Create(TEntity entity)
        {
            entity.Id = projectDbContext.SetOf<TEntity>().Count() + 1;
            projectDbContext.SetOf<TEntity>().Add(entity);
            await projectDbContext.SaveChangesAsync();
            return entity.Id;
        }

        public virtual async Task Delete(TEntity entity)
        {
            projectDbContext.SetOf<TEntity>().Remove(entity);
            await projectDbContext.SaveChangesAsync();
        }

        public virtual async Task DeleteById(int id)
        {
            projectDbContext.SetOf<TEntity>().Remove(projectDbContext.SetOf<TEntity>().Single(entity => entity.Id == id));
            await projectDbContext.SaveChangesAsync();
        }

        public virtual async Task<List<TEntity>> GetAll()
        {
            return projectDbContext.SetOf<TEntity>().ToList();
        }

        public virtual async Task<TEntity> GetById(int id)
        {
            return projectDbContext.SetOf<TEntity>().Single(entity => entity.Id == id);
        }

        public virtual async Task Update(TEntity entity)
        {
            var local = projectDbContext.SetOf<TEntity>().Local.FirstOrDefault(entry => entry.Id.Equals(entity.Id));
            if (local != null)
            {
                projectDbContext.Entry(local).State = EntityState.Detached;
            }
            projectDbContext.Entry(entity).State = EntityState.Modified;
            await projectDbContext.SaveChangesAsync();
        }
    }
}
