﻿using DAL.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DAL.DBInfrastructure
{
    public class ProjectDbContext : DbContext
    {
        public DbSet<Project> Projects { get; set; }
        public DbSet<Job> Jobs { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<User> Users { get; set; }

        public ProjectDbContext(DbContextOptions<ProjectDbContext> options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }

        public virtual void ChangeEntityState<TEntity>(TEntity entity, EntityState state) where TEntity : class
        {
            Entry(entity).State = state;
        }

        public DbSet<TEntity> SetOf<TEntity>() where TEntity : Entity
        {
            if (Jobs is IEnumerable<TEntity>)
            {
                Projects.Load();
                Users.Load();
                return Jobs as DbSet<TEntity>;
            }
            else if (Teams is IEnumerable<TEntity>)
            {
                Users.Load();
                Projects.Load();
                return Teams as DbSet<TEntity>;
            }
            else if (Projects is IEnumerable<TEntity>)
            {
                Jobs.Load();
                Teams.Load();
                Users.Load();
                return Projects as DbSet<TEntity>;
            }
            else
            {
                Jobs.Load();
                Teams.Load();
                Projects.Load();
                return Users as DbSet<TEntity>;
            }
        }
    }
}
