﻿using ClientApplication.Models;

namespace ClientApplication.SelectionModels
{
    public class JobsCount
    {
        public Project Project { get; set; }
        public int Count { get; set; }
    }
}
